from . import views
from django.urls import include
from django.contrib import admin
from django.urls import path, include
from .feeds import LatestPostsFeed, AtomSiteNewsFeed
from django.views.generic.base import TemplateView


urlpatterns = [
    
    path('accounts/', include('accounts.urls')), # new
    path('accounts/', include('django.contrib.auth.urls')),

    path("feed/rss", LatestPostsFeed(), name="post_feed"),
    path("feed/atom", AtomSiteNewsFeed()),
    path("", views.PostList.as_view(), name="home"),
    # path('<slug:slug>/', views.PostDetail.as_view(), name='post_detail'),
    path("<slug:slug>/", views.post_detail, name="post_detail"),
]

